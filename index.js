const express = require("express");
const app = express();

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
mongoose.connect(
  "mongodb+srv://admin:admin@cluster0.8j2zv.gcp.mongodb.net/library?retryWrites=true&w=majority"
);

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const cors = require("cors");
app.use(cors());

app.all("*", function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

Books = require("./api/models/books");
const routes = require("./api/routes/books");
routes(app);

const port = process.env.PORT || 5000;
app.listen(port);

console.log("Message RESTful API server started on: " + port);
