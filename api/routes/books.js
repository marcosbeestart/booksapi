"use strict";
module.exports = function(app) {
  const books = require("../controllers/books");

  app
    .route("/books")
    .get(books.getAll)
    .post(books.create);

  app
    .route("/books/:id")
    .get(books.get)
    .put(books.update)
    .delete(books.delete);
};
