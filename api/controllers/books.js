"use strict";
const mongoose = require("mongoose");
const Books = mongoose.model("Books");

exports.getAll = function(req, res) {
  Books.find({}, function(err, msg) {
    if (err) res.send(err);
    res.json(msg);
  });
};

exports.create = function(req, res) {
  const newBook = new Books(req.body);
  newBook.save(function(err, msg) {
    if (err) res.send(err);
    res.json(msg);
  });
};

exports.get = function(req, res) {
  Books.findById(req.params.id, function(err, msg) {
    if (err) res.send(err);
    res.json(msg);
  });
};

exports.update = function(req, res) {
  Books.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    function(err, msg) {
      if (err) res.send(err);
      res.json(msg);
    }
  );
};

exports.delete = function(req, res) {
  Books.remove({ _id: req.params.id }, function(err, msg) {
    if (err) res.send(err);
    res.json({ Book: "Book successfully deleted" });
  });
};
