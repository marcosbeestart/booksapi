"use strict";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var booksSchema = new Schema({
  author: { type: String },
  id: { type: String },
  name: { type: String },
  publisher: { type: String },
  year: { type: Number },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("Books", booksSchema);
